﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class AsteroidFactory : MonoBehaviour {

    public static AsteroidFactory Instance;
    public GameObject AsteroidPrefab;
    public Transform AsteroidParent;
    public AudioClip Division;
    
    private List<GameObject> _asteroids;

    private int _numberOfDivisions;
    private int _numberOfDivisionChildren;
    private int _numberOfInitialAsteroids;

    public void Awake()
    {
        Instance = this;
        _asteroids = new List<GameObject>();
    }

    public void InitLevel(int numDivisions, int numDivisionChildren, int numInitialAsteroids)
    {
        _numberOfDivisions = numDivisions;
        _numberOfDivisionChildren = numDivisionChildren;
        _numberOfInitialAsteroids = numInitialAsteroids;
    }


    public void CreateAsteroid()
    {
        CreateAsteroid(new Bounds());
    }

    public void CreateAsteroid(Bounds shipBounds)
    {
        var xside = Random.Range(0, 2) * 2 - 1;
        var yside = Random.Range(0, 2) * 2 - 1;
        var x = Random.Range(0f, Globals.ScreenHalfWidth) * xside;
        var y = Random.Range(0f, Globals.ScreenHalfHeight) * yside;
        var pos = new Vector3(x, y, 0);
        var rock = InstantiateAsteroid(pos, Quaternion.identity);

        if (shipBounds.size.magnitude == 0)
            AdjustForCollisionAtCreate(shipBounds, rock);

        var rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
        rock.GetComponent<Rigidbody2D>().velocity = rotation * rock.transform.up * Random.Range(0.5f, Globals.MaxInitialAsteroidSpeed);
        rock.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-360f, 360f);
        _asteroids.Add(rock);
    }

    public void CreateAsteroidsForLevel(Bounds shipBounds)
    {
        for (int i = 0; i < _numberOfInitialAsteroids; i++)
        {
            CreateAsteroid(shipBounds);
        }
    }

    private void AdjustForCollisionAtCreate(Bounds shipBounds, GameObject rock)
    {
        if (rock.GetComponent<Collider2D>().bounds.Intersects(shipBounds))
        {
            var newPos = new Vector3(rock.transform.position.x - shipBounds.size.x, rock.transform.position.y, 0f);
            rock.transform.position = newPos;
        }
    }

    public void RemoveAsteroid(GameObject asteroid)
    {
        var controller = asteroid.GetComponent<AsteroidController>();
        if (controller.DivisionNumber < _numberOfDivisions)
        {
            CreateDivisions(controller.DivisionNumber + 1, asteroid);    
        }
        _asteroids.Remove(asteroid);
        Destroy(asteroid);
        
    }

    private void CreateDivisions(int nextDivisionLevel, GameObject parentAsteroid)
    {
        SoundManager.Instance.AsteroidSfx(Division);
        for (int i = 0; i < _numberOfDivisionChildren; i++)
        {
            var rock = InstantiateAsteroid(parentAsteroid.transform.position, Quaternion.identity);
            var controller = rock.GetComponent<AsteroidController>();
            controller.Init(nextDivisionLevel);
            rock.transform.localScale = parentAsteroid.transform.localScale / 2;
            var rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            var body = rock.GetComponent<Rigidbody2D>();
            body.velocity = rotation * rock.transform.up * Random.Range(0.5f, 2f);
            body.angularVelocity = Random.Range(-360f, 360f);
            body.mass = parentAsteroid.GetComponent<Rigidbody2D>().mass / 2;
            _asteroids.Add(rock);
        }
    }

    private GameObject InstantiateAsteroid(Vector3 position, Quaternion rotation)
    {
        var rock = Instantiate(AsteroidPrefab, position, rotation) as GameObject;
        rock.name = "Asteroid";
        rock.transform.SetParent(AsteroidParent);
        return rock;
    }

    public bool ThereAreNoAsteroidsLeft()
    {
        return _asteroids.Count == 0;
    }

    public void ClearAsteroids()
    {
        foreach (var asteroid in _asteroids)
        {
            Destroy(asteroid);
        }
        _asteroids.Clear();
    }
}
