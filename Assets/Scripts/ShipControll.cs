﻿using UnityEngine;
using System.Collections;

public class ShipControll : MonoBehaviour , IDamageable {

    public float Velocity;
    public float RotationSpeed;
    public float ShieldHealth;

    public GameObject Shot;
    public Transform ShotSpawn;
    public ParticleSystem DieAnimation;
    public AudioSource Explosion;
    public AudioSource Alert;

    private Rigidbody2D _rigidbody2D;
    private bool _dead;
    
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    
    // Use this for initialization
	void Start () {
        ShieldHealth = 100;
        _dead = false;
        _rigidbody2D = GetComponent<Rigidbody2D>();
  	}
	
	// Update is called once per frame
    void Update()
    {

        if (ShieldHealth <= 0 && !_dead)
        {
            _dead = true;
            DieAnimation.Play();
            Explosion.Play();
            Alert.Play();
            GameManager.Instance.EnableState<GameOverState>();
        }

    }

    public void Reset()
    {
        transform.position = new Vector3();
        transform.rotation = new Quaternion();
        _rigidbody2D.velocity = new Vector2();
        _rigidbody2D.angularVelocity = 0f;

    }

	void FixedUpdate () {
        var AngularSpeed = _rigidbody2D.angularVelocity;
        var h = Input.GetAxisRaw("Horizontal");
        
        if (AngularSpeed < 100 && h < 0 || AngularSpeed > -100 && h > 0)
        {
            //_rigidbody2D.AddTorque(AngularVelocity * -h);            
            _rigidbody2D.rotation -= RotationSpeed*h; 
        }

        var v = Input.GetAxisRaw("Vertical");
        if (v == 1)
        {           
            _rigidbody2D.AddForce(transform.up * Velocity);
         
        }
	}


    public void GetShot(float damage)
    {
        //throw new System.NotImplementedException();
    }


    public void CollisionDamage(float damage)
    {
        ShieldHealth -= damage;
    }
}
