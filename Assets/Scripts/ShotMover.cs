﻿using UnityEngine;
using System.Collections;

public class ShotMover : MonoBehaviour
{

    public float Distance;
    public float Damage;
    public float FinalSpeed;
    
    private float TimeToLive;
    private float createdTime;
    private MetricsCollector _metrics;

    // Use this for initialization
    void Start()
    {
        createdTime = Time.time;
        TimeToLive = Distance / 10f;

        _metrics = MetricsCollector.Instance;
    }

    void FixedUpdate()
    {
        FinalSpeed = GetComponent<Rigidbody2D>().velocity.magnitude;
        if (Time.time - createdTime > TimeToLive)
            ShotFactory.Instance.RemoveShot(gameObject);
    }

    public void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag.ToLower() == "player")
            return;
        var damagable = coll.gameObject.GetComponent(typeof(IDamageable)) as IDamageable;
        if (damagable == null)
        {
            return;
        }

        damagable.GetShot(Damage);
        ShotFactory.Instance.RemoveShot(gameObject);
        _metrics.ShotHit();
    }



    


}
