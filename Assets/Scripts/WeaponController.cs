﻿using UnityEngine;
using System.Collections;

public class WeaponController : MonoBehaviour {

    private WeaponSpecsModel _weaponSpecs;
    private Rigidbody2D _shipBody;
    private Transform _shipTransform;
    private float _lastFired;
    

	// Use this for initialization
	void Start () {
        _weaponSpecs = GetComponent<WeaponSpecsModel>();
        _shipBody = GetComponentInParent<Rigidbody2D>();
        _shipTransform = GetComponentInParent<Transform>();
	}
	
	// Update is called once per frame
	void Update () {
	var fire = false;
        if (_weaponSpecs.HoldToFire)
            fire = Input.GetButton("Fire");
        else
            fire = Input.GetButtonDown("Fire");
        if (fire && TimeToFire())
        {

            switch (_weaponSpecs.Streams)
            {
                case 1:
                    FireSingle(_weaponSpecs.ShotSpawns[0]);
                    break;
                case 2:
                    FireSingle(_weaponSpecs.ShotSpawns[1]);
                    FireSingle(_weaponSpecs.ShotSpawns[2]);
                    break;
                case 3:
                    FireSingle(_weaponSpecs.ShotSpawns[0]);
                    FireSingle(_weaponSpecs.ShotSpawns[1]);
                    FireSingle(_weaponSpecs.ShotSpawns[2]);
                    break;
            }
            _lastFired = Time.realtimeSinceStartup;

        }
    
	}

    private void FireSingle(Transform spawn)
    {
        var clone = ShotFactory.Instance.CreateShot(transform, _weaponSpecs.MaxShotsOnScreen);
        if (clone != null)
        {
            clone.GetComponent<ShotMover>().Distance = _weaponSpecs.FiringDistance;
            var shotRigidBody = clone.GetComponent<Rigidbody2D>();
            shotRigidBody.velocity = _shipTransform.up.ToVector2() * 10 + _shipBody.velocity;
        }
    }

    private bool TimeToFire()
    {
        return (Time.realtimeSinceStartup - _lastFired) > _weaponSpecs.TimeBetweenShots;
    }

}
