﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UpdateHuntUI : MonoBehaviour {

    public Canvas canvas;

    private Text _textObject;

	// Use this for initialization
	void Start () {
        _textObject = canvas.GetComponentInChildren<Text>();
	}
	
	// Update is called once per frame
    void Update()
    {
        _textObject.text = string.Format(" {0}", GameManager.Instance.CrystalCount);
        if (AsteroidFactory.Instance.ThereAreNoAsteroidsLeft())
        {
            Application.LoadLevel("hq");
        }
    }
}
