﻿using UnityEngine;
using System.Collections;

public class AsteroidHuntState : MonoBehaviour {

    public GameObject PlayerShip;
    public GameObject Hud;

    private int _level;

    public void OnEnable()
    {
        _level = GameManager.Instance.GetLevel();
        SetupNewLevel(_level);
        MetricsCollector.Instance.StartLevel(_level);
    } 

	// Use this for initialization
	void Start () {

	
	}
	
	// Update is called once per frame
	void Update () {
        if (AsteroidFactory.Instance.ThereAreNoAsteroidsLeft())
        {
            Invoke("Transition", 4);
        }
	
	}

    private void SetupNewLevel(int level){
        var script = PlayerShip.GetComponent<ShipControll>();
        AsteroidFactory.Instance.InitLevel(DivisionCount(level), DivisionChildrenCount(level), InitAsteroidCount(level));
        AsteroidFactory.Instance.CreateAsteroidsForLevel(PlayerShip.GetComponent<Collider2D>().bounds);
        script.Reset();
        PlayerShip.SetActive(true);
        Hud.SetActive(true);
    }

    private int InitAsteroidCount(int level)
    {
        return Mathf.FloorToInt(2 + level * 0.1f);
    }

    private int DivisionCount(int level)
    {
        return 2;
    }

    private int DivisionChildrenCount(int level)
    {
        return Mathf.FloorToInt(2 + level * 0.05f);
    }

    void Transition()
    {
        MetricsCollector.Instance.EndLevel(_level);
        GameManager.Instance.EnableState<SummarScreen>();
        enabled = false;
    }


    
}
