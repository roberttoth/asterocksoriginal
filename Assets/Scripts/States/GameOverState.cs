﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverState : MonoBehaviour {

    public Text GameOverText;
    public Image WhiteFlash;
    public Image BlackBackground;
    public GameObject Canvas;
    public AudioSource Alert;
    public AudioSource Explosion;

    public float SecondsWhiteFlash;
    public float SecondsWhiteFade;
    public float SecondsBlackFadeIn;
    public float SecondsGameOverTextAppear;
    public float SecondsFadeOutAlert;

    private float _elapsed;
    private float _alertFadeoutElapsed;

	// Use this for initialization
	void Start () {
        //_hasBegun = false;
	}
	
	// Update is called once per frame
	void Update () {
        
        _elapsed += Time.unscaledDeltaTime;
        if (_elapsed < SecondsWhiteFlash)
            WhiteFlash.color = new Color(WhiteFlash.color.r, WhiteFlash.color.g, WhiteFlash.color.b, (_elapsed / SecondsWhiteFlash) * 1f);
        else 
        {
            WhiteFlash.color = new Color(WhiteFlash.color.r, WhiteFlash.color.g, WhiteFlash.color.b, 1f - (_elapsed / (SecondsWhiteFade+SecondsWhiteFlash)) * 1f);
            var blackAlpha = (_elapsed / (SecondsWhiteFlash+SecondsBlackFadeIn)) * 1f;
            BlackBackground.color = new Color(BlackBackground.color.r, BlackBackground.color.g, BlackBackground.color.b, blackAlpha);
            Time.timeScale = Mathf.Max((1f - blackAlpha*1.5f), 0f);
            Explosion.pitch = Time.timeScale;
        }

        if (_elapsed > SecondsWhiteFlash + SecondsBlackFadeIn / 2)
        {
            var alpha = ((_elapsed - SecondsWhiteFlash - SecondsBlackFadeIn/1.2f)/SecondsGameOverTextAppear) * 1f;
            GameOverText.color = new Color(GameOverText.color.r, GameOverText.color.g, GameOverText.color.b, alpha);
        }

        if (_elapsed > SecondsBlackFadeIn / 2)
        {
            _alertFadeoutElapsed += Time.unscaledDeltaTime;
            Alert.pitch = Mathf.Max(1f - ((_alertFadeoutElapsed / SecondsFadeOutAlert) * 1f),0);
            Alert.volume = Alert.pitch/1.2f;
        }


	}

    void OnEnable()
    {
        WhiteFlash.color = new Color(1f, 1f, 1f, 0);
        BlackBackground.color = new Color(0, 0, 0, 0);
        GameOverText.color = new Color(1f, 1f, 1f, 0);

        Canvas.SetActive(true);
        WhiteFlash.CrossFadeAlpha(1f, 10f, true);
    }

    void OnDisable()
    {
        if(Canvas != null)
            Canvas.SetActive(false);
    }
}
