﻿using UnityEngine;
using System.Collections;

public class HQState : MonoBehaviour {

    public ShipControll ShipController;
    public GameObject ExitPortal;
    
    public void OnEnable()
    {
        ShipController.Reset();
        ExitPortal.SetActive(true);
    }

    public void OnDisable()
    {
        ExitPortal.SetActive(false);
    }

    

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void PlayerHasExited()
    {
        GameManager.Instance.IncreaseLevel();
        GameManager.Instance.States.GetComponent<AsteroidHuntState>().enabled = true;
        enabled = false;
    }

    public void PlayerEnteredUpgradeStation()
    {
        GameManager.Instance.EnableState<UpgradeState>();
        enabled = false;
    }
}
