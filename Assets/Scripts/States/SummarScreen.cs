﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class SummarScreen : MonoBehaviour {

    public GameObject Canvas;
    public Text Accuracy;
    public Text TimeSpent;

    void OnEnable()
    {
        Canvas.SetActive(true);
        Accuracy.text = string.Format(Accuracy.text, (MetricsCollector.Instance.ShotsHit / MetricsCollector.Instance.ShotsFired)*100);
        var time = MetricsCollector.Instance.TimeToCompleteLevel;
        TimeSpent.text = string.Format("Time taken: {0:hh:mm:ss}", time);
    }

    void OnDisable()
    {
        Canvas.SetActive(false);
    }

    void Update()
    {
        if (Input.anyKeyDown)
        {
            Transition();
        }
    }

    void Transition()
    {
        GameManager.Instance.EnableState<HQState>();
        enabled = false;
    }
}
