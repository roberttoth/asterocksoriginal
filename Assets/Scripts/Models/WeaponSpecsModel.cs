﻿using System.Collections;
using UnityEngine;

public class WeaponSpecsModel : MonoBehaviour
    {
        
        public bool HoldToFire;
        public bool ExplodeOnImpact;
        public int MaxShotsOnScreen;
        public float FiringDistance;
        public int Streams;
        public float TimeBetweenShots;
        public Transform[] ShotSpawns;
    }
