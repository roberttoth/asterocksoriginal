﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class RapidFireUpgrade : MonoBehaviour {

    public WeaponSpecsModel Specs;
    public Button Button;


    public void RapidFire()
    {
        Specs.HoldToFire = true;
        Specs.MaxShotsOnScreen = 6;
        Button.enabled = false;
    }

    public void DoubleStreams()
    {
        Specs.Streams = 2;
        Specs.MaxShotsOnScreen = Mathf.Max(Specs.MaxShotsOnScreen, 6);
        Button.enabled = false;
    }




}
