﻿using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;

public class MetricsCollector : MonoBehaviour {

    public static MetricsCollector Instance { get; private set; }

    public float ShotsFired { get; private set; }
    public float ShotsHit { get; private set; }
    public TimeSpan TimeToCompleteLevel { get; private set; }

    private Stopwatch _timer;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    void Start()
    {
        _timer = new Stopwatch();
    }

    public void StartLevel(int level)
    {
        _timer.Reset();
        _timer.Start();
        ShotsFired = 0;
        ShotsHit = 0;
    }

    public void EndLevel(int level)
    {
        _timer.Stop();
        TimeToCompleteLevel = _timer.Elapsed;
    }

    public void ShotFired()
    {
        ShotsFired++;
    }

    public void ShotHit()
    {
        ShotsHit++;
    }
    
}
