﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

    public AudioSource Laser;
    public AudioSource Asteroid;
    public static SoundManager Instance = null;

    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else if (Instance != null)
            Destroy(gameObject);
    }

    public void LaserSfx(AudioClip clip)
    {
        Laser.clip = clip;
        Laser.Play();
    }

    public void AsteroidSfx(AudioClip clip)
    {
        Asteroid.clip = clip;
        Asteroid.Play();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
