﻿using UnityEngine;
using System.Collections;

public class BeamFire : MonoBehaviour {

    private LineRenderer beamRenderer;

    // Use this for initialization
	void Start () {
        beamRenderer = GetComponent<LineRenderer>();
        Debug.Log(string.Format("Renderer: {0}", beamRenderer));
	}
	
	// Update is called once per frame
	void Update () {
        var mouse = Input.GetMouseButton(0);
        if (mouse)
        {
            var mouseCoords = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            beamRenderer.SetPosition(0, transform.position);
            beamRenderer.SetPosition(1, mouseCoords);
            Debug.Log(string.Format("Beaming from {0} to {1}", transform.position, mouseCoords));
            beamRenderer.enabled = true;
        }
        else
        {
            beamRenderer.enabled = false;
        }
	
	}
}
